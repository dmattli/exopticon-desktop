/*
 * Exopticon - A free video surveillance system.
 * Copyright (C) 2022 David Matthew Mattli <dmm@mattli.us>
 *
 * This file is part of Exopticon.
 *
 * Exopticon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Exopticon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Exopticon.  If not, see <http://www.gnu.org/licenses/>.
 */

//! Exopticon is a free video surveillance system

#![deny(
    nonstandard_style,
    warnings,
    rust_2018_idioms,
    unused,
    future_incompatible,
    clippy::all,
    clippy::pedantic,
    clippy::nursery,
    clippy::cargo,
    clippy::unwrap_used
)]

use std::env;
use std::thread;
use std::time::Duration;

use anyhow::Error;
use base64::STANDARD;
use base64_serde::base64_serde_type;
use chrono::{DateTime, Utc};
use derive_more::{Display, Error};
use dotenv::dotenv;
use gstreamer::prelude::*;
use serde::{Deserialize, Serialize};
use tungstenite::client::IntoClientRequest;
use tungstenite::http::header::HeaderName;
use tungstenite::http::HeaderValue;
use tungstenite::{connect, Message};
use uuid::Uuid;

base64_serde_type!(Base64Standard, STANDARD);

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Hash, Serialize)]
pub enum FrameResolution {
    /// Standard definition frame, 480p
    SD,
    /// High definition frame, camera native resolution
    HD,
}

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Hash, Serialize)]
pub enum SubscriptionSubject {
    /// A camera id and frame resolution
    Camera(i32, FrameResolution),
    /// Analysis engine id
    AnalysisEngine(i32),
    /// Playback id, name, initial video unit id, initial offset
    Playback(u64, i32, i64),
}

#[derive(Serialize, Deserialize)]
pub enum WsCommand {
    /// Subscription request
    Subscribe(SubscriptionSubject),
    /// Unsubscription request
    Unsubscribe(SubscriptionSubject),
    /// frame ack response
    Ack,
}

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Hash, Serialize)]
#[serde(rename_all = "camelCase")]
#[serde(tag = "kind")]
pub enum FrameSource {
    /// Camera with camera id
    Camera {
        /// id of camera
        #[serde(rename = "cameraId")]
        camera_id: i32,
        analysis_offset: Duration,
    },
    /// Analysis Engine, with engine id
    AnalysisEngine {
        /// id of source analysis engine
        #[serde(rename = "analysisEngineId")]
        analysis_engine_id: i32,
        analysis_offset: Duration,
    },
    /// Video Playback
    Playback {
        /// Playback id, must be unique per socket
        id: u64,
    },
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Observation {
    /// id of Observation
    pub id: i64,
    /// offset from beginning of video unit, starts at 0
    pub frame_offset: i64,
    /// Identifies the type of observation, eg Person, Motion, Deer
    pub tag: String,
    /// Details associated with observation, eg John, Male, whatever
    pub details: String,
    /// A value between 0-100 representing the percentage certainty of
    /// the observation.
    pub score: i16,
    /// upper-left x coordinate
    pub ul_x: i16,
    /// upper-left y coordinate
    pub ul_y: i16,
    /// lower-right x coordinate
    pub lr_x: i16,
    /// lower-right y coordinate
    pub lr_y: i16,
    /// Time that observation record was inserted
    pub inserted_at: DateTime<Utc>,
    /// id of owning video unit
    pub video_unit_id: Uuid,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RawCameraFrame {
    /// id of camera that produced frame
    pub camera_id: i32,
    /// resolution of frame
    pub resolution: FrameResolution,
    /// original width of frame
    pub unscaled_width: i32,
    /// original height of frame
    pub unscaled_height: i32,
    /// source of frame
    pub source: FrameSource,
    /// id of video unit
    pub video_unit_id: Uuid,
    /// offset from beginning of video unit
    pub offset: i64,
    /// observations associated with frame
    pub observations: Vec<Observation>,
    /// jpeg image data
    #[serde(with = "Base64Standard")]
    pub jpeg: Vec<u8>,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
#[serde(tag = "kind")]
pub enum WsMessage {
    /// A frame of video
    Frame(RawCameraFrame),
    /// Message indicating the end of playback
    PlaybackEnd {
        /// if of playback session to end
        id: u64,
    },
}

fn exopticon_connect(appsrc: gstreamer_app::AppSrc) {
    thread::spawn(move || {
        let ws_uri = env::var("EXOPTICON_WS_URL")
            .expect("EXOPTICON_WS_URL must be set")
            .parse::<http::Uri>()
            .expect("failed to parse websocket uri");
        println!("My scheme! {:?}", ws_uri.scheme_str());
        let mut ws_request: tungstenite::handshake::client::Request = ws_uri
            .into_client_request()
            .expect("Failed to create client request");
        let token = HeaderValue::from_str(
            &env::var("EXOPTICON_TOKEN").expect("EXOPTICON_TOKEN must be set"),
        )
        .expect("Failed to parse header value");
        let header_name = HeaderName::from_static("private-key");
        let headers = ws_request.headers_mut();

        headers.append(header_name, token);

        let (mut socket, _response) = connect(ws_request).expect("Can't connect");
        println!("I'm the websocket thread!!");

        let camera_id = env::var("CAMERA_ID")
            .expect("CAMERA_ID must be set")
            .parse::<i32>()
            .expect("Failed to parse camera id");
        let message =
            WsCommand::Subscribe(SubscriptionSubject::Camera(camera_id, FrameResolution::HD));
        let message_json =
            serde_json::to_string(&message).expect("Failed to serialize subscription request");
        socket
            .write_message(Message::Text(message_json))
            .expect("Failed to send subscription request");
        let caps = &gstreamer::Caps::builder("image/jpeg")
            .field("framerate", gstreamer::IntRange::<i32>::new(1, i32::MAX))
            .build();
        appsrc.set_format(gstreamer::Format::Time);
        loop {
            let msg = socket.read_message().expect("Error reading message");
            let ack_json =
                serde_json::to_string(&WsCommand::Ack).expect("failed to serialize ack message");

            if let Message::Text(msg) = msg {
                let deserialized: WsMessage =
                    serde_json::from_str(&msg).expect("failed to deserialize WsMessage");
                match deserialized {
                    WsMessage::Frame(frame) => {
                        let mut buffer = gstreamer::Buffer::with_size(frame.jpeg.len())
                            .expect("failed to create image buffer");

                        let bufmut = buffer.get_mut().expect("failed to get image buffer");
                        bufmut
                            .copy_from_slice(0, &frame.jpeg)
                            .expect("failed to copy image buffer");
                        appsrc.set_caps(Some(caps));

                        let _ = appsrc.push_buffer(buffer);
                    }
                    WsMessage::PlaybackEnd { id: _id } => {}
                }
                socket
                    .write_message(Message::Text(ack_json))
                    .expect("failed to write ack message");
            }
        }
    });
}

#[derive(Debug, Display, Error)]
#[display(fmt = "Missing element {}", _0)]
struct MissingElement(#[error(not(source))] &'static str);

#[derive(Debug, Display, Error)]
#[display(fmt = "Received error from {}: {} (debug: {:?})", src, error, debug)]
struct ErrorMessage {
    src: String,
    error: String,
    debug: Option<String>,
    source: glib::Error,
}

fn create_pipeline() -> Result<gstreamer::Pipeline, Error> {
    gstreamer::init()?;

    let pipeline = gstreamer::Pipeline::new(None);
    let src =
        gstreamer::ElementFactory::make("appsrc", None).map_err(|_| MissingElement("appsrc"))?;
    let jpegparse = gstreamer::ElementFactory::make("jpegparse", None)
        .map_err(|_| MissingElement("jpegparse"))?;
    let jpegdecode = gstreamer::ElementFactory::make("jpegdec", None)
        .map_err(|_| MissingElement("jpegdecode"))?;
    let videoconvert = gstreamer::ElementFactory::make("videoconvert", None)
        .map_err(|_| MissingElement("videoconvert"))?;
    let sink = gstreamer::ElementFactory::make("autovideosink", None)
        .map_err(|_| MissingElement("autovideosink"))?;

    sink.set_property("sync", false);

    pipeline.add_many(&[&src, &jpegparse, &jpegdecode, &videoconvert, &sink])?;
    gstreamer::Element::link_many(&[&src, &jpegparse, &jpegdecode, &videoconvert, &sink])?;

    let appsrc = src
        .dynamic_cast::<gstreamer_app::AppSrc>()
        .expect("Source element is expected to be an appsrc!");

    thread::spawn(move || {
        exopticon_connect(appsrc);
    });

    Ok(pipeline)
}

#[allow(clippy::needless_pass_by_value)]
fn main_loop(pipeline: gstreamer::Pipeline) -> Result<(), Error> {
    pipeline.set_state(gstreamer::State::Playing)?;

    let bus = pipeline
        .bus()
        .expect("Pipeline without bus. Shouldn't happen!");

    for msg in bus.iter_timed(gstreamer::ClockTime::NONE) {
        use gstreamer::MessageView;

        match msg.view() {
            MessageView::Eos(..) => break,
            MessageView::Error(err) => {
                pipeline.set_state(gstreamer::State::Null)?;
                return Err(ErrorMessage {
                    src: msg
                        .src()
                        .map_or_else(|| String::from("None"), |s| String::from(s.path_string())),
                    error: err.error().to_string(),
                    debug: err.debug(),
                    source: err.error(),
                }
                .into());
            }
            _ => (),
        }
    }

    pipeline.set_state(gstreamer::State::Null)?;

    Ok(())
}

fn main() {
    dotenv().ok();

    match create_pipeline().and_then(main_loop) {
        Ok(r) => r,
        Err(e) => eprintln!("Error! {}", e),
    }
}
